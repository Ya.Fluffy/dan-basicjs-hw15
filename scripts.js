// Теоретичні питання
// 1. Як можна визначити, яку саме клавішу клавіатури натиснув користувач?
// за допомогою події 'keyup', 'keydown' та властивості key або keyCode
// 2. Яка різниця між event.code() та event.key()?
// key повертає символ, а event.code() код кнопки
// 3. Які три події клавіатури існує та яка між ними відмінність?
// Події 
// - keydown  виникає, коли клавішу натиснуто,
// - keyup - коли клавішу відпускають.
// -keypress - коли клавіша зажата.




// Практичне завдання.
// Реалізувати функцію підсвічування клавіш.


// Технічні вимоги:
// - У файлі index.html лежить розмітка для кнопок.

// - Кожна кнопка містить назву клавіші на клавіатурі

// - Після натискання вказаних клавіш - та кнопка, на якій написана ця літера, повинна фарбуватися в синій колір. При цьому якщо якась інша літера вже раніше була пофарбована в синій колір - вона стає чорною. Наприклад за натисканням Enter перша кнопка забарвлюється у синій колір. Далі, користувач натискає S, і кнопка S забарвлюється в синій колір, а кнопка Enter знову стає чорною.

// Але якщо при натискані на кнопку її не існує в розмітці, то попередня активна кнопка повина стати неактивною.

const btns = document.querySelectorAll('.btn')

 window.addEventListener('keyup', (e) => {
    console.log(e.key, e.code)
    for( let btn of btns) {
        console.log(btn)
        btn.style.backgroundColor = ''
        if(e.key.toUpperCase() === btn.textContent.toUpperCase() || (btn.textContent === 'Tab' && e.key === 'Backspace')){
            btn.style.backgroundColor = 'blue'
        } else {
            btn.style.backgroundColor = ''
        }
    }
 })



//  const buttons = document.querySelectorAll('.btn');

// window.addEventListener('keydown', (event) => {
//     // console.log(event.key, event.code)
//     debugger
//   const pressedKey = event.key;
//     const buttonToColor = Array.from(buttons).find((button) => {
//         return button.textContent === pressedKey;
//     });
//     buttons.forEach((button) => {
//         
//         if (buttonToColor) {
// console.log('button', button)
// console.log('buttonToColor', buttonToColor)

//             buttonToColor.style.backgroundColor = 'blue';
//              button.style.backgroundColor = '';
//         } else if (!buttonToColor) {
//             button.style.backgroundColor = '';
//         }
//     })
// });
// window.addEventListener('keypress', (e)=>{
//     console.log('keypress')
// })
// window.addEventListener('keyup', (e)=>{
//     console.log('keyup')
// })
// window.addEventListener('keydown', (e)=>{
//     console.log('keydown')
// })















//? class work

// document.addEventListener('scroll', (e)=>{
//     console.log(e)
//     let scrollTop = document.documentElement.scrollTop;
//     let scrollLeft = document.documentElement.scrollTop;
// const first = document.querySelector('div[0]');
// console.log(first)

// })


/**
 * Завдання 1.
 *
 * Створити елемент h1 з текстом «Натисніть будь-яку клавішу».
 *
 * При натисканні будь-якої клавіші клавіатури змінювати текст елемента h1 на:
 * «Натиснена клавіша: ІМ'Я_КЛАВИШІ».
 */


// const h1 = document.createElement('h1')
// h1.textContent = 'Натисніть будь-яку клавішу';
// document.body.append(h1)

// document.body.addEventListener('keyup',(e)=>{
//     console.log(e.key)
//     h1.textContent = `Натиснена клавіша: ${e.key}`;
// })

/**
 * Завдання 2.
 *
 * При натисканні shift та "+"" одночасно збільшувати
 * шрифт сторінки на 1px
 * а при shift та "-" - зменшувати на 1px
 *
 * Максимальний розмір шрифту - 30px, мінімальний - 10px
 *
 */
// let fontSize = 16;

// window.addEventListener('keyup', (e) => {
//     console.log(e.key, e.code)
//     if(e.shiftKey && e.key === '+'){
//         if(fontSize > 30){
//             return
//         }
//         console.log(e.key, document.body.style.fontSize)
//         fontSize++
//         document.body.style.fontSize = `${fontSize}px`
//     } else
//     if(e.shiftKey && e.code === 'Minus'){
//         if(fontSize < 10){
//             return
//         }

//         console.log(e.key, document.body.style.fontSize)
//         fontSize--
//         document.body.style.fontSize = `${fontSize}px`
//     }

// })

/**
 * Завдання 3.
 *
 * Рахувати кількість натискань на пробіл, ентер,
 * та Backspace клавіші
 * Відображати результат на сторінці
 *
 * ADVANCED: створити функцію, яка приймає тільки
 * назву клавіші, натискання якої потрібно рахувати,
 * а сам лічильник знаходиться в замиканні цієї функції
 * (https://learn.javascript.ru/closure)
 * id елемента, куди відображати результат має назву
 * "KEY-counter"
 *
 * Наприклад виклик функції
 * createCounter('Enter');
 * реалізовує логіку підрахунку натискання клавіші Enter
 * та відображає результат в enter-counter блок
 *
 */
// let counterEnter = 0;
// let counterSpace = 0;
// let counterBackspase = 0

// const div = document.createElement('div')
// div.innerHTML=`
//     <p>Enter pressed: <span id="enter-counter">${counterEnter}</span></p>
//     <p>Space pressed: <span id="space-counter">${counterSpace}</span></p>
//     <p>Backspace pressed: <span id="backspace-counter">${counterBackspase}</span></p>
// `
// document.body.append(div)
// const backspace = document.querySelector('#backspace-counter')
// const space = document.querySelector('#space-counter')
// const enter = document.querySelector('#enter-counter')


// window.addEventListener('keyup', (e)=>{
//     console.log(e.key)
//     console.log(e.code)

//     if(e.key === 'Backspace'){
//         counterBackspase++
//         backspace.textContent = counterBackspase
//     } else if(e.code === 'Space'){
//         counterSpace++
//         space.textContent = counterSpace

//     } else if(e.code === 'Enter'){
//         counterEnter++
//         enter.textContent = counterEnter
//     }
// })

const form = document.querySelector('form')
form.addEventListener('submit', (e)=> {
    e.preventDefault()
    console.log(e.target)
})